/**
 * @author Cynthia Rangel
 * @version 2017
 */

import a_star.*;

public class Main{
    public static void main(String[] args) {
        Node[] n = new Node[10];
        for (int i = 0; i < n.length; i++) {
            n[i] = new Node();
            n[i].setData("n-" + i);
        }
        /*
         * X = Paredes
         * N1 => Inicio
         * N8 => Meta
         *
        N0 - N3 - N5 - N8
        |         |
        N1   X    N6    X
        |         |
        N2 - N4 - N7 - N9

         */

        n[0].setXY(0, 0);
        n[1].setXY(0, 1);
        n[2].setXY(0, 2);
        n[3].setXY(1, 0);
        n[4].setXY(1, 2);
        n[5].setXY(2, 0);
        n[6].setXY(2, 1);
        n[7].setXY(2, 2);
        n[8].setXY(3, 0);
        n[9].setXY(3, 2);

        n[0].addNeighbors(n[1], n[3]);
        n[1].addNeighbors(n[0], n[2]);
        n[2].addNeighbors(n[1], n[4]);
        n[3].addNeighbors(n[0], n[5]);
        n[4].addNeighbors(n[2], n[7]);
        n[5].addNeighbors(n[3], n[8]);
        n[6].addNeighbors(n[7], n[5]);
        n[7].addNeighbors(n[4], n[9], n[6]);
        n[8].addNeighbors(n[5]);
        n[9].addNeighbor(n[7]);

        AStar search = new AStar();
        search.traverse(n[1], n[8]);
    }
}